#!/bin/bash
# wykys 2020

###############################################################################

REPO_DIR="miner"
MINER_DIR="app"
MINER_APP="ethdcrminer64"
MINER_ZIP="app.zip"

###############################################################################

if [ $(hostname) == "wpc" ] ; then
    WORK_DIR="$HOME/projects"
else
    WORK_DIR="$HOME/data"
fi

###############################################################################

function RunMiner {
screen -dmS miner $WORK_DIR/$REPO_DIR/$MINER_DIR/$MINER_APP -dcri 5
}

###############################################################################

function Help {
echo "Help:"
echo "    -h, --help               show this help message and exit"
echo "    -s, --start              start miner"
echo "    -i, --install CUBE_NAME  installs the mainer"
}

###############################################################################

CMD=$1

if [ "$CMD" == "--help" ] || [ "$CMD" == "-h" ] ; then

Help

###############################################################################

elif [ "$CMD" == "--start" ] || [ "$CMD" == "-s" ] ; then

RunMiner

###############################################################################

elif [ "$CMD" == "--install" ] || [ "$CMD" == "-i" ] ; then

SERVER=$2

cd $WORK_DIR/$REPO_DIR
rm -rf $MINER_DIR

# download miner
wget -O $MINER_ZIP https://github.com/Claymore-Dual/Claymore-Dual-Miner/releases/download/15.0/Claymore.s.Dual.Ethereum.AMD+NVIDIA.GPU.Miner.v15.0.-.LINUX.zip
unzip -o $MINER_ZIP
rm -rf $MINER_ZIP

mv "Claymore's Dual Ethereum AMD+NVIDIA GPU Miner v15.0 - LINUX" $MINER_DIR
cd $MINER_DIR
chmod +x ./$MINER_APP

# setting the main cryptocurrency
MINER_CONFIG_EPOOLS="epools.txt"
echo "
POOL: stratum+tcp://daggerhashimoto.eu.nicehash.com:3353, WALLET: 3ALUqWqDdtZy4ArM3hSzLfMj8qetn1Rtbq.$SERVER, PSW: x, ESM: 3, ALLPOOLS: 1, ALLCOINS: 1
POOL: stratum+tcp://daggerhashimoto.usa.nicehash.com:3353, WALLET: 3ALUqWqDdtZy4ArM3hSzLfMj8qetn1Rtbq.$SERVER, PSW: x, ESM: 3, ALLPOOLS: 1, ALLCOINS: 1
" > $MINER_CONFIG_EPOOLS

# setting the secondary cryptocurrency
MINER_CONFIG_DPOOLS="dpools.txt"
echo "
POOL: stratum+tcp://decred.eu.nicehash.com:3354, WALLET: 3ALUqWqDdtZy4ArM3hSzLfMj8qetn1Rtbq.$SERVER, PSW: x
POOL: stratum+tcp://decred.usa.nicehash.com:3354, WALLET: 3ALUqWqDdtZy4ArM3hSzLfMj8qetn1Rtbq.$SERVER, PSW: x
" > $MINER_CONFIG_DPOOLS

echo "#!/bin/bash
$WORK_DIR/$REPO_DIR/miner.sh --start
" > $WORK_DIR/startup.bash
chmod +x $WORK_DIR/startup.bash

RunMiner

###############################################################################

else
echo "Error: Incorrect input!"
Help
fi
